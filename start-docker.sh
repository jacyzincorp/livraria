#!/bin/bash

# package the WAR file
# mvn package

# Make sure docker instance is running
# boot2docker up

# Build a new docker image
docker build -t jacyzin/tomcat9:dev .

# Run the application
docker run --rm -p 8080:8080 jacyzin/tomcat9:dev

